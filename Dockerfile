# All dockerfiles must begin with FROM
FROM node:10.13-alpine

# RUN mkdir -p /usr/src/app/node_modules && chown -R node:node /usr/src/app

ENV NODE_ENV production

WORKDIR /usr/src/app

COPY package*.json ./

USER node

RUN npm install --production --silent

COPY --chown=node:node . .

EXPOSE 1741

CMD node server.js